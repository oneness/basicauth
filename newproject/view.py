#coding: utf-8

from django.http import HttpResponse
import json
import base64

def get_basic_auth_str(username, password):
    temp_str = username + ':' + password
    # 转成bytes string
    bytesString = temp_str.encode(encoding="utf-8")
    # base64 编码
    encodestr = base64.b64encode(bytesString)
    # 解码
    decodestr = base64.b64decode(encodestr)
 
    return 'Basic ' + encodestr.decode()

def action(request):
    request.encoding='utf-8'
    user = request.GET.get('user')
    password = request.GET.get('password')

    _user_auth_str = request.META.get('HTTP_AUTHORIZATION')
#    if user == None or password == None:
#        return HttpResponse("Username or password can not be empty!", status="400")

    if _user_auth_str == get_basic_auth_str('user1', 'user1'):
        resp = {'sub': 'user1',  'name': 'user1'}
    elif _user_auth_str == get_basic_auth_str('user2', 'user2'):
        resp = {'sub': 'user2',  'name': 'user2'}
    else:
        return HttpResponse("Username or password is wrong!", status="401")
    
    return HttpResponse(json.dumps(resp), content_type="application/json")

def hello(request):
    return HttpResponse("Hello world ! ")
